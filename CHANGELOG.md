### 1.0.0 (2022-11-28)

* Initial release

### 1.0.1 (2022-11-28)

* Streamlined directory structure

### 1.0.2 (2022-11-28)

* Added missing step to installation instructions

### 1.0.3 (2022-12-02)

* Simplified installation instructions

### 1.0.4 (2022-12007)

* Fixed broken factory insertion of itself as a dependency

