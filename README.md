# Slipstream - Automatic dependency injection for PHP

Slipstream automates dependency injection allowing you to add or remove dependencies in real time without modifying any other classes. Dependencies are typically injected through the __construct method of a class using type hinting. Alternatively, they can be injected through an inject() method.
## Installation

Install the latest version with

```bash
$ composer require ooblatron/slipstream -o
```

Please note the `-o` flag. This generates the Composer autoload class map that is required by Slipstream.

## Basic Usage

```php
<?php

use Slipstream\SlipstreamFactory;

$vendorPath = '../vendor/'; // Change this to the location of your Composer vendor directory
$classLoader = require $vendorPath.'autoload.php';
$factory = new SlipstreamFactory($classLoader);
$newObject = $factory->get('newObject');
```
Objects are created with all dependencies automatically injected, without you having to supply them or update Pimple
registration details.

#### PHP 7.4 example of a class with dependencies

```php
<?php

class NewObject
{
    private Dependency1 $dependency1;
    private Dependency2 $dependency2;
    
    public function __construct(
        Dependency1 $dependency1,
        Dependency2 $dependency2    
    ): {
      $this->dependency1 = $dependency1;
      $this->dependency2 = $dependency2;
    }
    
    public function someMethod() {
        $this->dependency1->doSomething();
    }
}    
```

#### PHP 8.1 example

```php
<?php

class NewObject
{
    public function __construct(
        private Dependency1 $dependency1,
        private Dependency2 $dependency2    
    ): { }
    
    public function someMethod() {
        $this->dependency1->doSomething();
    }
}    
```

#### Inject dependencies into an object after it has been created
This is useful for third party classes (e.g. Laravel models) whose dependencies cannot be injected through the
__construct() method). This example depends on your Factory having been created already (see above).

```php
<?php

class ExistingObject
{
    public function inject(
        private Dependency1 $dependency1,
        private Dependency2 $dependency2    
    ): { }
    
    public function someMethod() {
        $this->dependency1->doSomething();
    }
}    
```

```php
<?php

use Slipstream\Slipstream;

class ClassUsingExistingObject
{
    public function __construct(
        Slipstream $slipstream,
        ExistingObject $existingObject
    ) {
        $this->slipstream = $slipstream;
        $this->existingObject = $existingObject;
    }
    
    public function methodThatUsesExistingObject() {
        // Inject dependencies into ExistingObject
        $this->slipstream->inject($this->existingObject);
    }
}
```

### Adding new classes

Slipstream relies on the Composer autoload class map to identify dependencies. Therefore, the following command must be
run whenever you create or rename a class or change the namespace of a class.

```bash
composer dump-autoload -o
```

## Documentation

- [Please visit the Slipstream website](https://ooblatron.org/slipstream)

## About

### Requirements

- PHP 7.1 or above

### How it works

PHP Reflection is used in conjunction with the Composer autoloader map to identify classes that are dependencies for the
class being instantiated. These are iteratively instantiated and injected into the new object.

### Author

Tim Rogers - <tim@ooblatron.org> - <https://twitter.com/timdrogers><br />

### License

Slipstream is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

### Acknowledgements

#### Laravel

This library was inspired by the way that Laravel automatically injects dependencies into controller methods.

#### Pimple

This library uses the [Pimple dependency manager](https://packagist.org/packages/pimple/pimple) to hold instantiated classes for reuse and injection.