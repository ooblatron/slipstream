<?php

namespace Slipstream;

use Pimple\Container;

interface Registrar
{
    function register(Container $_container): Container;
}