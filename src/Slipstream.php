<?php


namespace Slipstream;


use Exception;
use Pimple\Container;
use ReflectionClass;
use ReflectionException;
use ReflectionNamedType;
use ReflectionParameter;
use function in_array;
use function lcfirst;
use function str_replace;
use function strrpos;
use function substr;
use function ucwords;

/**
 * Class Slipstream
 * This class instantiates classes and injects dependencies.
 * @package Slipstream
 */
class Slipstream
{
    /*
     * https://www.php.net/manual/en/language.types.intro.php
     */
    private const NON_OBJECT_TYPE_NAMES = [
        'bool',
        'int',
        'float',
        'double',
        'string',
        'array',
        'object',
        'callable',
        'iterable'
    ];

    public const CONSTRUCTOR_DEPENDENCIES = '__construct';
    public const INJECTED_DEPENDENCIES = 'inject';
    private const INJECT_METHOD_NAME = 'inject';
    /**
     * @var Container $container
     */
    private $container;
    /**
     * @var array $dependencyStack
     */
    private $dependencyStack;
    /**
     * @var array $aliases
     */
    private $aliases = array();
    /**
     * @var array $replicants
     */
    private $replicants = array();

    public function __construct()
    {
        $this->reset();
    }

    /**
     * Reset
     * Creates a new container and clears the dependency stack.
     * @return void
     */
    public function reset(): void {
        $this->container = new Container();
        // Register self
        $this->register('slipstream', $this);
    }

    /**
     * Get Dependencies
     * Uses Reflection to return the dependencies of the specified class. The dependencies are defined in the
     * parameters of the class's __construct method. These parameters must all be type-hinted, otherwise an exception
     * is thrown. Parameters that are not objects are allowed if they are optional, otherwise an exception is thrown.
     * @param string $_fullClassName
     * @return array
     * @throws ReflectionException
     * @throws Exception
     */
    public function getDependencies(string $_fullClassName): array
    {
        $dependencies = array();
        $dependencies[self::CONSTRUCTOR_DEPENDENCIES] = array();
        $dependencies[self::INJECTED_DEPENDENCIES] = array();
        $reflector = new ReflectionClass($_fullClassName);
        if (!in_array($_fullClassName, $this->dependencyStack, true)) {
            $this->dependencyStack[] = $_fullClassName;
        } else {
            throw new Exception('Circular dependency found when creating '.$_fullClassName);
        }

        // Get constructor dependencies (if any)
        if ($reflector->getConstructor()) {
            $parameters = $reflector->getConstructor()->getParameters();
            $dependencies[self::CONSTRUCTOR_DEPENDENCIES] = $this->getParameterDependencies(
                $parameters,
                $_fullClassName
            );
        }

        // Get injected dependencies (if any)
        if ($reflector->hasMethod(self::INJECT_METHOD_NAME)) {
            $parameters = $reflector->getMethod(self::INJECT_METHOD_NAME)->getParameters();
            $dependencies[self::INJECTED_DEPENDENCIES] = $this->getParameterDependencies(
                $parameters,
                $_fullClassName
            );
        }

        return $dependencies;
    }

    /**
     * Get Parameter Dependencies
     * Returns an array of dependencies specified by the passed parameters.
     * @throws Exception
     **/
    private function getParameterDependencies(
        array $_parameters,
        string $_fullClassName
    ): array
    {
        $dependencies = array();
        foreach ($_parameters as $parameter) {
            $object = $this->getDependency(
                $parameter,
                $_fullClassName
            );
            if ($object) {
                $dependencies[] = $object;
            }
        }

        return $dependencies;
    }

    /**
     * Get Dependency
     * Checks whether the specified parameter can be instantiated as an object. If it can be, returns an instantiated
     * object. Otherwise, either returns null if the parameter is optional and is not an object (e.g. it is an array) or
     * throws an exception.
     * @param ReflectionParameter $_parameter
     * @param string $_fullClassName
     * @return object|null
     * @throws Exception
     */
    private function getDependency(
        ReflectionParameter $_parameter,
        string $_fullClassName
    ) {
        $object = null;
        $injectedDependencyTypeName = $this->getInjectedDependencyTypeName($_parameter);
        $injectedDependencyName = $_parameter->getName();
        $isAnObject = !in_array($injectedDependencyTypeName, self::NON_OBJECT_TYPE_NAMES);

        // Throw an error if there is a reason this dependency cannot be created
        $this->validateDependency(
            $_parameter->hasType(),
            $_parameter->allowsNull(), // todo In PHP 8 replace with $_parameter->hasDefaultValue()
            $isAnObject,
            $_parameter->isOptional(),
            $_fullClassName,
            $injectedDependencyName,
            $injectedDependencyTypeName
        );

        if ($isAnObject and !is_null($injectedDependencyTypeName)) {
            // Attempt to create dependency and throw an error if not successful
            try {
                $object = $this->getInstantiatedDependency($injectedDependencyTypeName);
            } catch (Exception $e) {
                throw new Exception(
                    $_fullClassName .
                    ' constructor ' . $injectedDependencyTypeName .
                    ' parameter: $' . $injectedDependencyName . ' cannot be created because '.$e
                );
            }
        }

        return $object;
    }

    /**
     * @param $_injectedDependency
     * @return string|null
     */
    private function getInjectedDependencyTypeName($_injectedDependency) : ?string
    {
        $injectedDependencyTypeName = null;
        if ($_injectedDependency->hasType()) {
            /** @var ReflectionNamedType $injectedDependencyType */
            $injectedDependencyType = $_injectedDependency->getType();
            $injectedDependencyTypeName = $injectedDependencyType->getName();
        }

        return $injectedDependencyTypeName;
    }

    /**
     * Validate Dependency
     * Checks whether it is possible to create the passed dependency and throws an error if it is not.
     * @param bool $_hasType
     * @param bool $_allowsNull
     * @param bool $_isAnObject
     * @param bool $_isOptional
     * @param string $_fullClassName
     * @param string $_injectedDependencyName
     * @param string|null $_injectedDependencyTypeName
     * @throws Exception
     */
    private function validateDependency(
        bool $_hasType,
        bool $_allowsNull,
        bool $_isAnObject,
        bool $_isOptional,
        string $_fullClassName,
        string $_injectedDependencyName,
        ?string $_injectedDependencyTypeName
    ) : void
    {
        if (!$_hasType and !$_allowsNull) {
            throw new Exception(
                $_fullClassName . ' constructor has untyped parameter: $' . $_injectedDependencyName
            );
        }
        if (!$_isAnObject and !$_isOptional and !$_allowsNull) {
            throw new Exception(
                $_fullClassName .
                ' constructor has non-optional ' . $_injectedDependencyTypeName .
                ' parameter: $' . $_injectedDependencyName
            );
        }
    }

    /**
     * Get Instantiated Dependency
     * Gets an instance of the specified class. If such an object has already been created, and it is a singleton,
     * returns the object from the Pimple Container. Otherwise, creates a new object and stores that in the Pimple
     * Container for future use.
     * @param string $_fullClassName Fully qualified class name with namespace
     * @return object|null
     * @throws ReflectionException
     * @throws Exception
     */
    public function getInstantiatedDependency(
        string $_fullClassName
    ) {
        $containerName  = $this->getContainerName($_fullClassName);
        if ($this->isReplicant($containerName)) {
            $dependency = $this->getFromContainer($containerName) ?? $this->getNewReplicant($_fullClassName);
        } else {
            $dependency = $this->getFromContainer($containerName) ?? $this->getNewSingleton($_fullClassName);
        }

        return $dependency;
    }
    /**
     * Get New Replicant
     * Uses Reflection to create a new non-singleton object, fully injected with its dependencies, and stores it in the
     * Pimple container for reuse.
     * @param string $_fullClassName
     * @return object|null
     * @throws ReflectionException
     */
    private function getNewReplicant(string $_fullClassName) {
        $containerName = $this->getContainerName($_fullClassName);
        $dependencies = $this->getDependencies($_fullClassName);
        $this->addFactoryToContainer(
            $containerName,
            $_fullClassName,
            $dependencies
        );

        return $this->getFromContainer($containerName);
    }

    /**
     * Get New Singleton
     * Uses Reflection to create a new singleton object, fully injected with its dependencies, and stores it in the
     * Pimple container for reuse.
     * @param string $_fullClassName
     * @return object|null
     * @throws ReflectionException
     */
    private function getNewSingleton(string $_fullClassName) {
        $containerName = $this->getContainerName($_fullClassName);
        $dependencies = $this->getDependencies($_fullClassName);
        $this->addSingletonToContainer(
            $containerName,
            $_fullClassName,
            $dependencies
        );

        return $this->getFromContainer($containerName);
    }

    /**
     * Get Container Name
     * Returns the name of the class that has been registered with the Pimple Container. This is normally the class
     * name, but can be set to something else by adding a row to the alias constant array.
     * @param string $_fullClassName Fully qualified class name with namespace
     * @return string
     */
    public function getContainerName(string $_fullClassName): string
    {
        $className = $this->getClassName($_fullClassName);

        return $this->aliases[$_fullClassName] ?? $this->camelCase($className);
    }

    /**
     * Get From Container
     * Returns the specified object from the Pimple container. If no such object has been registered, returns null.
     * @param string $_containerName
     * @param string|null $_creationData
     * @return object|string|null
     */
    public function getFromContainer(
        string $_containerName,
        ?string $_creationData = null
    )
    {
        if ($this->container->offsetExists($_containerName)) {
            $this->container[$_containerName . 'CreationData'] = $_creationData;
            return $this->container[$_containerName];
        } else {
            return null;
        }
    }

    /**
     * Is Replicant
     * Returns true if the class with the specified container name is not a singleton (defined by being specified in the
     * class property $factories array), otherwise returns false.
     * @param string $_containerName
     * @return bool
     */
    public function isReplicant(string $_containerName): bool
    {
        return in_array($_containerName, $this->replicants, true);
    }

    /**
     * Add Singleton To Container
     * Registers the new object with the Pimple Container to save having to create it again. This is only done if the
     * object has not already been registered (which is possible if a new instance of a class is created using the
     * getNewObject() method after a singleton has been created using the getObject() method).
     * @param string $_containerName
     * @param string $_fullClassName
     * @param array $_dependencies
     */
    public function addSingletonToContainer(
        string $_containerName,
        string $_fullClassName,
        array $_dependencies
    ) : void
    {
        $constructorDependencies = $_dependencies[self::CONSTRUCTOR_DEPENDENCIES];
        $injectedDependencies = $_dependencies[self::INJECTED_DEPENDENCIES];

        if (!$this->container->offsetExists($_containerName)) {
            // Class needs to be registered
            if (!empty($constructorDependencies) and !empty($injectedDependencies)) {
                // Class has both constructor and injected dependencies
                $this->container[$_containerName] =
                    function () use ($_fullClassName, $constructorDependencies, $injectedDependencies) {
                        $object = new $_fullClassName(...$constructorDependencies);
                        $object->inject(...$injectedDependencies);
                        return $object;
                    };
            } elseif (!empty($constructorDependencies) and empty($injectedDependencies)) {
                // Class has constructor dependencies but no injected dependencies
                $this->container[$_containerName] = function () use ($_fullClassName, $constructorDependencies) {
                    return new $_fullClassName(...$constructorDependencies);
                };
            } elseif (empty($constructorDependencies) and !empty($injectedDependencies)) {
                // Class has injected dependencies but no constructor dependencies
                $this->container[$_containerName] = function () use ($_fullClassName, $injectedDependencies) {
                    $object = new $_fullClassName();
                    $object->inject(...$injectedDependencies);
                    return $object;
                };
            } else {
                // Class has no constructor or injected dependencies
                $this->container[$_containerName] = function () use ($_fullClassName) {
                    return new $_fullClassName();
                };
            }
        }
    }

    /**
     * Add SlipstreamFactory to Container
     * Registers the new object with the Pimple Container as a factory. Any future instances fetched from the container
     * will be new instances of the class and not singletons.
     * @param string $_containerName
     * @param string $_fullClassName
     * @param array $_dependencies
     */
    public function addFactoryToContainer(
        string $_containerName,
        string $_fullClassName,
        array $_dependencies
    ) : void
    {
        $constructorDependencies = $_dependencies[self::CONSTRUCTOR_DEPENDENCIES];
        $injectedDependencies = $_dependencies[self::INJECTED_DEPENDENCIES];

        if (!$this->container->offsetExists($_containerName)) {
            // Class needs to be registered
            if (!empty($constructorDependencies) and !empty($injectedDependencies)) {
                // Class has both constructor and injected dependencies
                $this->container[$_containerName] = $this->container->factory(
                    function () use ($_fullClassName, $_dependencies, $constructorDependencies, $injectedDependencies) {
                        $object = new $_fullClassName(...$constructorDependencies);
                        $object->inject(...$injectedDependencies);
                        return $object;
                    });
            } elseif (!empty($constructorDependencies) and empty($injectedDependencies)) {
                // Class has constructor dependencies but no injected dependencies
                $this->container[$_containerName] = $this->container->factory(
                    function () use ($_fullClassName, $constructorDependencies) {
                        return new $_fullClassName(...$constructorDependencies);
                    });
            } elseif (empty($constructorDependencies) and !empty($injectedDependencies)) {
                // Class has injected dependencies but no constructor dependencies
                $this->container[$_containerName] = $this->container->factory(
                    function () use ($_fullClassName, $injectedDependencies) {
                        $object = new $_fullClassName();
                        $object->inject(...$injectedDependencies);
                        return $object;
                    });
            } else {
                // Class has no constructor or injected dependencies
                $this->container[$_containerName] = $this->container->factory(function () use ($_fullClassName) {
                    return new $_fullClassName();
                });
            }
        }
    }

    /**
     * Inject
     * Injects dependencies into the passed object through its inject() method and returns the object.
     * @param object|null $_object
     * @return object|null
     * @throws ReflectionException
     */
    public function inject($_object)
    {
        $this->clearDependencyStack();
        if ($_object) {
            $fullClassName = get_class($_object);
            $reflector = new ReflectionClass($fullClassName);

            if ($reflector->hasMethod(self::INJECT_METHOD_NAME)) {
                $dependencies = $this->getDependencies($fullClassName);
                if ($dependencies[self::INJECTED_DEPENDENCIES]) {
                    $_object->inject(...$dependencies[self::INJECTED_DEPENDENCIES]);
                }
            }
        }

        return $_object;
    }

    /**
     * Get Class Name
     * Returns just the class name without the namespace.
     * https://www.php.net/manual/en/function.get-class.php#114568
     * @param string $_fullClassNameWithNamespace
     * @return string
     */
    private function getClassName(string $_fullClassNameWithNamespace): string
    {
        if ($pos = strrpos($_fullClassNameWithNamespace, '\\')) {
            $className = substr($_fullClassNameWithNamespace, $pos + 1);
        } else {
            $className = $_fullClassNameWithNamespace;
        }

        return $className;
    }

    /**
     * Camel Case
     * Converts snake or kebab case string to camel case.
     * @param string $_string
     * @return string
     */
    public function camelCase(string $_string): string
    {
        return lcfirst($this->pascalCase($_string));
    }

    /**
     * Pascal Case
     * Converts snake or kebab case string to Pascal case.
     * Adapted from https://codereview.stackexchange.com/a/125664
     * @param string $_string
     * @return string
     */
    public function pascalCase(string $_string): string
    {
        return str_replace(' ', '', ucwords(str_replace(['-', '_'], ' ', $_string)));
    }

    public function register(string $string, $param): void
    {
        $this->container[$string] = $param;
    }

    public function setContainer(Container $_container): void
    {
        $this->container = $_container;
    }

    public function getContainer(): Container
    {
        return $this->container;
    }

    public function clearDependencyStack(): void
    {
        $this->dependencyStack = array();
    }

    public function setAliases(array $_aliases): void {
        $this->aliases = $_aliases;
    }

    public function setReplicants(array $_replicants): void {
        $this->replicants = $_replicants;
    }

    public function getAliases(): array {
        return $this->aliases;
    }
}