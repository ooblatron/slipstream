<?php


namespace Slipstream;

use Composer\Autoload\ClassLoader;
use Exception;
use ReflectionException;

/**
 * Class SlipstreamFactory
 * Initialises dependency injection mapping and creates objects with fully injected dependencies.
 * todo Throw an exception if a replicant has previously been incorrectly registered in the container as a singleton
 * todo Throw an exception if a class's inject() method does not return $this
 * @package Slipstream
 */
class SlipstreamFactory
{
    /**
     * @var ClassLoader $classLoader
     */
    private $classLoader;
    /**
     * @var Slipstream $slipstream
     */
    protected $slipstream;
    /**
     * @var string
     */
    private $topLevelNamespaceName;

    /**
     * SlipstreamFactory constructor.
     */
    public function __construct(ClassLoader $_classLoader)
    {
        $this->classLoader = $_classLoader;
        $this->slipstream = new Slipstream();
        $this->registerSelf();
        return $this;
    }

    public function reset(): void
    {
        $this->slipstream->reset();
        $this->registerSelf();
    }

    private function registerSelf(): void {
        // Register self
        $this->slipstream->register('factory', $this);
        // Register ClassLoader
        $this->slipstream->register('classLoader', $this->classLoader);
    }

    /**
     * Get
     * Returns an instance of the class with the specified container name. This is normally a singleton (unless the
     * container name is specified in the class property $factories array). Use the method getNewObject() instead if a
     * new instance and not a singleton is needed.
     *
     * An optional substitute container name can be specified. This will store the created object under a different name
     * in the Pimple container. This can be used for testing - e.g. to replace an object with a test object.
     * @param string $_containerName
     * @param string|null $_creationData
     * @param string|null $_substituteContainerName
     * @return object
     * @throws ReflectionException
     * @throws Exception
     */
    public function get(
        string $_containerName,
        ?string $_creationData = null,
        ?string $_substituteContainerName = null
    ) {
        $this->slipstream->clearDependencyStack();
        $object = $this->slipstream->getFromContainer(
            $_containerName,
            $_creationData
        );
        if (is_null($object)) {
            $fullClassName = $this->getFullClassName($_containerName);
            $containerName = $this->slipstream->getContainerName($fullClassName);
            $dependencies = $this->slipstream->getDependencies($fullClassName);
            if (!$this->slipstream->isReplicant($_containerName)) {
                $this->slipstream->addSingletonToContainer(
                    $_substituteContainerName ?? $containerName,
                    $fullClassName,
                    $dependencies
                );
           } else {
                $this->slipstream->addFactoryToContainer(
                    $_substituteContainerName ?? $containerName,
                    $fullClassName,
                    $dependencies
                );
            }
            $object = $this->slipstream->getFromContainer($_substituteContainerName ?? $containerName);
        }

        return $object;
    }

    /**
     * Get Full Class Name
     * Returns the class name with full namespace
     * See https://stackoverflow.com/a/15887310/2511355
     * @param string $_containerName
     * @return string
     * @throws Exception
     */
    public function getFullClassName(string $_containerName): string
    {
        $fullClassName = array_search(
            $this->slipstream->camelCase($_containerName),
            $this->slipstream->getAliases()
        );
        if ($fullClassName === false) {
            // There is no alias, so look up the full class name in Composer's autoloader class map file
            $className = $this->slipstream->pascalCase($_containerName);
            // Get full class names and remove any aliases
            $classMap = array_diff($this->classLoader->getClassMap(), array_keys($this->slipstream->getAliases()));
            $searchPattern = '/' . $this->getTopLevelNamespaceName() . '(\\\\.*)*\\\\' . $className . '$/';
            $matches = preg_grep($searchPattern, array_keys($classMap));

            if (count($matches) == 0) {
                throw new Exception('Class '.$className.' not found in ' . $this->getTopLevelNamespaceName() .
                    ' namespace');
            }
            if (count($matches) > 1) {
                throw new Exception('Class '.$className.' is ambiguous in ' .
                    $this->getTopLevelNamespaceName() .
                    ' namespace ('.count($matches).' found), SlipstreamFactory alias needed');
            }

            $fullClassName = current($matches);
        }

        return $fullClassName;
    }

    /**
     * Camel Case
     * Converts snake or kebab case string to camel case.
     * @param string $_string
     * @return string
     */
    public function camelCase(string $_string): string
    {
        return $this->slipstream->camelCase($_string);
    }

    /**
     * Get New Object
     * This returns a new instance of the class with the specified container name. This method should be used when a
     * singleton is not required, otherwise use the getObject() method.
     * @param string $_containerName
     * @return object
     * @throws ReflectionException
     * @throws Exception
     */
    public function getNewObject(
        string $_containerName
    ) {
        $fullClassName = $this->getFullClassName($_containerName);
        $dependencies = $this->slipstream->getDependencies($fullClassName);
        return $this->getNewInstance(
            $fullClassName,
            $dependencies
        );
    }

    /**
     * Get New Instance
     * Returns a new instance of the specified class fully injected with the specified dependencies.
     * @param string $_fullClassName
     * @param array $_dependencies
     * @return object
     */
    private function getNewInstance(
        string $_fullClassName,
        array $_dependencies
    ) {
        $constructorDependencies = $_dependencies[$this->slipstream::CONSTRUCTOR_DEPENDENCIES];
        $injectedDependencies = $_dependencies[$this->slipstream::INJECTED_DEPENDENCIES];

        if (!empty($constructorDependencies) and !empty($injectedDependencies)) {
            // Class has both constructor and injected dependencies
            return (new $_fullClassName(...$constructorDependencies))->inject(...$injectedDependencies);
        } elseif (!empty($constructorDependencies) and empty($injectedDependencies)) {
            // Class has constructor dependencies but no injected dependencies
            return new $_fullClassName(...$constructorDependencies);
        } elseif (empty($constructorDependencies) and !empty($injectedDependencies)) {
            // Class has injected dependencies but no constructor dependencies
            return (new $_fullClassName())->inject(...$injectedDependencies);
        } else {
            // Class has no constructor or injected dependencies
            return new $_fullClassName();
        }
    }

    /**
     * Register Manual Dependencies
     * Registers dependencies that cannot be created and injected using Reflection by this factory. These are typically
     * third party classes that do not have typed constructor parameters and therefore cannot be created automatically.
     */
    protected function registerManualDependencies(Registrar $_registrar) : void
    {
        $this->slipstream->setContainer($_registrar->register($this->slipstream->getContainer()));
    }

    /**
     * Set Top Level Namespace Name
     * @param string $_namespaceName
     * @return void
     */
    public function setTopLevelNamespaceName(string $_namespaceName): void {
        $this->topLevelNamespaceName = $_namespaceName;
    }
    
    /**
     * Get Top Level Namespace Name
     * @return string
     */
    protected function getTopLevelNamespaceName(): string {
        return $this->topLevelNamespaceName ?? '';
    }
}